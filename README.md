# Running

```bash
// install dependencies
$ npm i

// run server
$ npm run start
```

# Configuration

Server support configuration vie `.env`(just rename `.env.example`) or via env vars.

```
// Port to bind
PORT=80 
// private key of sys account where __PART__ is part of key without 0x
ETH_ACCOUNT_PRIVATE_KEY=0x__PART__
// not used now
ETH_PER_REQUEST_SEND_LIMIT
// http provider for web3
ETH_NET_PROVIDER=https://rinkeby.infura.io/v3/__API-KEY__
```

# Endpoints

 * *POST* **/address/{address}/airdrop** - `address` is eth account address with `0x`.

Send amount ETHs to `address`.

Request body:
```js
{
  "currencyCode": "ETH",
  // amount to send: number, string
  "amount": 0.1
}
```
