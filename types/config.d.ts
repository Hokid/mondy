export interface Config {
    PORT: number | string;
    ETH_ACCOUNT_PRIVATE_KEY: string;
    ETH_PER_REQUEST_SEND_LIMIT: number | string | void | null;
    ETH_NET_PROVIDER: string;
}
