export interface ApiError {
    readonly errors: string[];
    readonly code: number;
    readonly additional?: { [key: string]: any };
}
