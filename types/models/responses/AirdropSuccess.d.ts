import {TransactionReceipt} from "web3/types";

export interface AirdropSuccessD {
    readonly receipt: TransactionReceipt;
}
