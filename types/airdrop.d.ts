import BN from 'bignumber.js';
import {TransactionReceipt} from "web3/types";

export interface Airdrop {
    send(to: string, value: number | string | BN): Promise<TransactionReceipt>;
}
