import Web3 = require("web3");
import {Airdrop} from "./airdrop";
import {Logger} from "winston";

export interface Services {
    web3: Web3;
    airdrop: Airdrop;
    logger: Logger;
}
