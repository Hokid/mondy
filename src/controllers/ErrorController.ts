import {BaseController} from "./Base";
import {Response, Request, NextFunction} from "express";
import {ApiError} from "../models/ApiError";
import {Logger} from "winston";

export class ErrorController extends BaseController {
    private logger: Logger;

    constructor(logger: Logger) {
        super();

        this.logger = logger;
    }

    handler(error: Error | ApiError, req: Request, res: Response, next: NextFunction) {
        if (error) {
            this.logger.error(error);

            if (error instanceof Error) {
                this.logger.debug(error);
            }

            if (!(error instanceof ApiError)) {
                error = new ApiError({
                    code: 500,
                    errors: [(<Error>error).message]
                });
            }

            return this.send(res, error.code || 500, error);
        }

        next();
    }
}
