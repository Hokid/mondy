import {BaseController} from "./Base";
import {Request, Response, NextFunction} from 'express';
import {ApiError} from "../models/ApiError";


export class NotFoundController extends BaseController {
    handler(req: Request, res: Response, next: NextFunction) {
        next(new ApiError({
            code: 404,
            errors: ['NotFound']
        }));
    }
}
