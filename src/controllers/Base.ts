import {Request, Response} from 'express';


export class BaseController {
    sendOk(res: Response, data: any) {
        return res.status(200).json(data);
    }

    sendBad(res: Response, data: any) {
        return res.status(400).json(data);
    }

    sendServerError(res: Response, data: any) {
        return res.status(500).json(data);
    }

    send(res: Response, status: number, data: any) {
        return res.status(status).json(data);
    }
}
