import {NextFunction, Request, Response} from 'express';
import {Airdrop} from "../../types/airdrop";
import {Logger} from "winston";
import Web3 = require("web3");
import BN from 'bignumber.js';
import {ApiError} from "../models/ApiError";
import {BaseController} from "./Base";
import {AirdropSuccess} from "../models/responses/AirdropSuccess";


export class AirdropController extends BaseController {
    private airdrop: Airdrop;
    private logger: Logger;
    private web3: Web3;

    constructor(airdrop: Airdrop, logger: Logger, web3: Web3) {
        super();

        this.airdrop = airdrop;
        this.logger = logger;
        this.web3 = web3;
    }

    async airdropHandler(req: Request, res: Response, next: NextFunction) {
        const errors = this.validateInput(req.params, req.body);

        if (errors.length) {
            return next(new ApiError({
                code: 400,
                errors
            }));
        }

        const to = req.params.address as string;
        const value = req.body.amount as string;

        try {
            const receipt = await this.airdrop.send(to, value);

            if (!receipt.status) {
                return next(new ApiError({
                    code: 400,
                    errors: ['airdrop fail'],
                    additional: {
                        receipt
                    }
                }));
            }

            this.sendOk(res, new AirdropSuccess({
                receipt
            }));
        } catch (e) {
            return next(new ApiError({
                code: 400,
                errors: [e.message]
            }));
        }
    }

    private validateInput(params: any, body: any) {
        const errors = [];

        if (!this.web3.utils.isAddress(params.address)) {
            errors.push('address is not valid');
        }

        if (!body) {
            errors.push('body is empty');
        }

        const value = new BN(body.amount);

        if (value.isNaN() || !value.isFinite()) {
            errors.push('wrong amount');
        }

        if (body.currencyCode !== 'ETH') {
            errors.push('wrong currency code');
        }

        return errors;
    }
}
