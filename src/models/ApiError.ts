import {ApiError as ApiErrorInterface} from "../../types/models/ApiError";

export class ApiError implements ApiErrorInterface {
    readonly errors: string[];
    readonly code: number;
    readonly additional?: { [key: string]: any };

    constructor(model: ApiErrorInterface) {
        this.code = model.code;
        this.errors = model.errors;

        if (model.hasOwnProperty('additional')) {
            this.additional = model.additional;
        }
    }
}
