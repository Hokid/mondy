import {AirdropSuccessD as AirdropSuccessInterface} from "../../../types/models/responses/AirdropSuccess";
import {TransactionReceipt} from "web3/types";


export class AirdropSuccess implements AirdropSuccessInterface {
    readonly receipt: TransactionReceipt;

    constructor(model: AirdropSuccessInterface) {
        this.receipt = model.receipt;
    }
}
