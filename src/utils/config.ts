import {Config} from "../../types/config";

export function resolve(config: any): Readonly<Config> {
    const result: Partial<Config> = {};

    result.PORT = config.PORT != null
        ? config.PORT
        : (process.env.PORT || 80);
    result.ETH_ACCOUNT_PRIVATE_KEY =
        config.ETH_ACCOUNT_PRIVATE_KEY != null
            ? config.ETH_ACCOUNT_PRIVATE_KEY
            : (process.env.ETH_ACCOUNT_PRIVATE_KEY || '');
    result.ETH_PER_REQUEST_SEND_LIMIT =
        config.ETH_PER_REQUEST_SEND_LIMIT != null
            ? config.ETH_PER_REQUEST_SEND_LIMIT
            : (process.env.ETH_PER_REQUEST_SEND_LIMIT || null);
    result.ETH_NET_PROVIDER =
        config.ETH_NET_PROVIDER != null
            ? config.ETH_NET_PROVIDER
            : (process.env.ETH_NET_PROVIDER || null);

    return result as Readonly<Config>;
}

export function validate(config: Readonly<Config>): boolean {
    if (config.PORT == null) return false;
    if (!config.ETH_ACCOUNT_PRIVATE_KEY) return false;
    if (!config.ETH_NET_PROVIDER) return false;

    return true;
}
