import {App} from "./app";
import path from 'path';
import {config} from 'dotenv';
import {resolve, validate} from "./utils/config";

const envpath = path.resolve(process.cwd(), '.env');
const configuration = config({
    path: envpath,
    encoding: 'utf8'
});

const appConfig = resolve(configuration.parsed || {});

if (!validate(appConfig)) {
    throw new Error('not valid configuration provided');
}

const app = new App(appConfig);

app.start();
