import express from "express";
import bodyParser from "body-parser";
import {Config} from "../types/config";
import {Services} from "../types/services";
import Web3 = require("web3");
import logger from './services/Logger';
import {Airdrop} from "./services/Airdrop";
import {AirdropController} from "./controllers/AirdropController";
import {ErrorController} from "./controllers/ErrorController";
import {NotFoundController} from "./controllers/NotFoundController";


export class App {
    public readonly app: express.Application;
    private readonly config: Readonly<Config>;
    private services!: Services;
    private airdropController!: AirdropController;
    private notFoundController!: NotFoundController;
    private errorController!: ErrorController;

    constructor(appConfig: Readonly<Config>) {
        this.config = appConfig;
        this.app = express();
        this.loadServices();
        this.loadControllers();
        this.setupExpress();
    }

    private loadServices(): void {
        const web3 = this.createWeb3();
        const airdrop = new Airdrop(this.config, web3, logger);

        this.services = {
            web3,
            airdrop,
            logger
        };
    }

    private loadControllers(): void {
        this.airdropController = new AirdropController(
            this.services.airdrop,
            this.services.logger,
            this.services.web3
        );
        this.notFoundController = new NotFoundController();
        this.errorController = new ErrorController(this.services.logger);
    }

    private setupExpress(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));

        this.app.route('/address/:address/airdrop')
            .post(this.airdropController.airdropHandler.bind(this.airdropController));

        this.app.use(this.notFoundController.handler.bind(this.notFoundController));
        this.app.use(this.errorController.handler.bind(this.errorController));
    }

    private createWeb3() {
        return new Web3(new Web3.providers.HttpProvider(this.config.ETH_NET_PROVIDER));
    }

    public start(): void {
        this.app.listen(this.config.PORT, () => {
            this.services.logger.info('app is listening on port %d', this.config.PORT);
        });
    }
}
