import {Config} from "../../types/config";
import BN from 'bignumber.js';
import Web3 = require("web3");
import {TransactionReceipt} from "web3/types";
import {Airdrop as AirdropInterface} from "../../types/airdrop";
import {Logger} from "winston";
import {Signature} from "web3/eth/accounts";

export class Airdrop implements AirdropInterface {
    private readonly web3: Web3;
    private readonly logger: Logger;
    private readonly privateKey: string;
    private readonly address: string;
    private readonly limitPerRequest: BN | null;

    constructor(config: Readonly<Config>, web3: Web3, logger: Logger) {
        this.web3 = web3;
        this.logger = logger;
        this.privateKey = config.ETH_ACCOUNT_PRIVATE_KEY;
        this.address = this.web3.eth.accounts.privateKeyToAccount(this.privateKey).address;
        this.limitPerRequest = config.ETH_PER_REQUEST_SEND_LIMIT != null
            ? new BN(config.ETH_PER_REQUEST_SEND_LIMIT)
            : null;
    }

    public async send(to: string, value: number | string | BN): Promise<TransactionReceipt> {
        this.logger.debug('[Airdrop.send]: to: %s, value: %s', to, value);

        value = new BN(this.web3.utils.toWei(value.toString(), 'ether'));

        if (!this.web3.utils.isAddress(to)) {

            throw new Error('`to` must be a valid eth address');
        }

        const balance = await this.web3.eth.getBalance(this.address);

        this.logger.debug('[Airdrop.send]: balance: %s', balance);

        if (value.gt(balance)) {
            throw new Error('account balance is not enough to airdrop');
        }

        const signedTransaction = await this.getSignedTransaction(to, value);
        const receipt = await this.web3.eth.sendSignedTransaction(signedTransaction);

        this.logger.debug(receipt);

        this.logger.debug('[Airdrop.send]: success');

        return receipt;
    }

    private async getSignedTransaction(to: string, value: number | string | BN): Promise<string> {
        const gas = await this.web3.eth.estimateGas({ to, value: value.toString() });
        const result = await this.web3.eth.accounts.signTransaction({ to, value: value.toString(), gas }, this.privateKey);

        return (<{ rawTransaction: string } & Signature>result).rawTransaction;
    }
}
