FROM node:carbon

ENV PORT=80
ENV ETH_ACCOUNT_PRIVATE_KEY=0x__PART__
ENV ETH_PER_REQUEST_SEND_LIMIT=''
ENV ETH_NET_PROVIDER=https://rinkeby.infura.io/v3/__API-KEY__

ARG PORT=$PORT

WORKDIR /app

RUN npm install npm@latest -g && npm -v && node -v

COPY . .

RUN npm ci

EXPOSE $PORT

CMD [ "npm", "run", "start" ]


